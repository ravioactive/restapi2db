﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace gracenote1.data
{
    class DataPusher : DataWorkerBase
    {

        private string connectionStr = null;

        public DataPusher()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["gracenote1.Properties.Settings.connString"];
            if (settings != null)
            {
                connectionStr = settings.ConnectionString;
            }
            if (connectionStr == null)
            {
                Console.WriteLine("\r\n\t Connection String is null. ");
            }
            
        }

        public List<model.RootObject> readObjectsFromXML(string name)
        {
            if (name == null || name.Length == 0)
            {
                Console.WriteLine("\r\n\t name of the file is blank or null.");
                return null;
            }
            string filename = name + ".xml";
            string relativePath = ReadSetting("storage");
            string xmlStoragePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
            if (!System.IO.Directory.Exists(xmlStoragePath))
            {
                Console.WriteLine("\r\n\t Directory does not exist. Cannot read file.");
                return null;
            }

            xmlStoragePath += filename;
            if (!System.IO.File.Exists(xmlStoragePath))
            {
                Console.WriteLine("\r\n\t File with name: " + filename + " does not exist. Cannot read file.");
                return null;
            }
            List<model.RootObject> readObjects = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<model.RootObject>));
                using (System.IO.TextReader reader = new System.IO.StreamReader(xmlStoragePath))
                {
                    readObjects = (List<model.RootObject>)serializer.Deserialize(reader);
                }
            }
            catch (System.Exception se)
            {
                Console.WriteLine("\r\n\t Exception in reading XML file at location \r\n\t " + xmlStoragePath);
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                return null;
            }

            return readObjects;
        }

        private int store(model.RootObject r, SqlConnection sqlConn)
        {
            if (r == null || r.tmsId == null || r.tmsId.Length < 1) 
            {
                Console.WriteLine("\tListing has illegal (null) values for tmsId. Discarding. \r\n\t " + r.ToString());
                return -1; 
            }
            //Console.WriteLine(r.ToString());
            model.PreferredImage pi = r.preferredImage;
            int pid = -1;
            if (pi != null)
            {
                pid = storePreferredImage(pi, sqlConn);
            }

            List<model.Showtime> stl = r.showtimes;
            List<int> sids = null;
            if (stl != null && stl.Count > 0)
            {
                sids = storeShowTimes(stl, sqlConn);
            }

            model.QualityRating qr = r.qualityRating;
            int qrid = -1;
            if (qr != null)
            {
                qrid = storeQualityRaing(qr, sqlConn);
            }

            List<model.Rating> rs = r.ratings;
            List<int> rids = null;
            if (rs != null && rs.Count > 0)
            {
                rids = storeRatings(rs, sqlConn);
            }

            int rid = -1;
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.Listing where tmsId like @tmsId AND rootId like @rootid", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@tmsId", r.tmsId);
                    sqlCommand.Parameters.AddWithValue("@rootId", r.rootId == null || r.rootId.Length == 0 ? DBNull.Value.ToString() : r.rootId);
                    object result = (int)sqlCommand.ExecuteScalar();
                    int rcount = (result == null) ? 0 : (int)result;
                    sqlConn.Close();
                    if (rcount <= 0)
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.Listing (tmsId, rootId, subType, title, releaseYear, releaseDate, titleLang, descriptionLang, entityType, genres, directors, " + 
                            "longDescription, shortDescription, topCast, advisories, runtime, preferredimageid, showtimes, ratings, officialurl, animation) " +
                            "VALUES (@tmsId, @rootId, @subType, @title, @releaseYear, @releaseDate, @titleLang, @descriptionLang, @entityType, @genres, @directors, @longDescription, @shortDescription, @topCast, @advisories, @runtime, @preferredimageid, " +
                            "@showtimes, @ratings, @officialurl, @animation); SELECT CAST(scope_identity() AS int);", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@tmsId", r.tmsId);
                            insert_sqlCommand.Parameters.AddWithValue("@rootId", r.rootId == null || r.rootId.Length == 0 ? DBNull.Value.ToString() : r.rootId);
                            insert_sqlCommand.Parameters.AddWithValue("@subType", r.subType == null || r.subType.Length == 0 ? DBNull.Value.ToString() : r.subType);
                            insert_sqlCommand.Parameters.AddWithValue("@title", r.title == null || r.title.Length == 0 ? DBNull.Value.ToString() : r.title);
                            
                            insert_sqlCommand.Parameters.AddWithValue("@releaseYear", r.releaseYear);

                            if (r.releaseDate != null && r.releaseDate.Length > 0)
                            {
                                SqlParameter dateParam = new SqlParameter("@releaseDate", SqlDbType.Date);
                                DateTime d = DateTime.ParseExact(r.releaseDate, new[] { "yyyy-MM-dd", "yyyy" }, null, DateTimeStyles.None);
                                dateParam.Value = d;
                                insert_sqlCommand.Parameters.Add(dateParam);
                            }
                            else
                            {
                                insert_sqlCommand.Parameters.AddWithValue("@releaseDate", DBNull.Value);
                            }
                            
                            insert_sqlCommand.Parameters.AddWithValue("@titleLang", r.titleLang == null || r.titleLang.Length == 0 ? DBNull.Value.ToString() : r.titleLang);
                            insert_sqlCommand.Parameters.AddWithValue("@descriptionLang", r.descriptionLang == null || r.descriptionLang.Length == 0 ? DBNull.Value.ToString() : r.descriptionLang);
                            insert_sqlCommand.Parameters.AddWithValue("@entityType", r.entityType == null || r.entityType.Length == 0 ? DBNull.Value.ToString() : r.entityType);
                            string genreStr = "";
                            foreach(string g in r.genres) { genreStr += g; genreStr += ","; }
                            insert_sqlCommand.Parameters.AddWithValue("@genres", genreStr.Length == 0 ? DBNull.Value.ToString() : genreStr);
                            string dirStr = "";
                            foreach(string ds in r.directors) { dirStr += ds; dirStr += ","; }
                            insert_sqlCommand.Parameters.AddWithValue("@directors", dirStr.Length == 0 ? DBNull.Value.ToString() : dirStr);
                            insert_sqlCommand.Parameters.AddWithValue("@longDescription", r.longDescription == null || r.longDescription.Length == 0 ? DBNull.Value.ToString() : r.longDescription);
                            insert_sqlCommand.Parameters.AddWithValue("@shortDescription", r.shortDescription == null || r.shortDescription.Length == 0 ? DBNull.Value.ToString() : r.shortDescription);
                            string tcStr = "";
                            foreach (string tc in r.topCast) { tcStr += tc; tcStr += ","; }
                            insert_sqlCommand.Parameters.AddWithValue("@topCast", tcStr.Length == 0 ? DBNull.Value.ToString() : tcStr);
                            string adStr = "";
                            foreach (string ad in r.advisories) { adStr += ad; adStr += ","; }
                            insert_sqlCommand.Parameters.AddWithValue("@advisories", adStr.Length == 0 ? DBNull.Value.ToString() : adStr);
                            insert_sqlCommand.Parameters.AddWithValue("@runtime", r.runTime == null || r.runTime.Length == 0 ? DBNull.Value.ToString() : r.runTime);
                            
                            insert_sqlCommand.Parameters.AddWithValue("@preferredimageid", (pid>0) ? pid : 1);
                            string showStr = "";
                            if (sids != null)
                            {
                                foreach (int s in sids) { showStr += s.ToString(); showStr += ","; }
                            }
                            insert_sqlCommand.Parameters.AddWithValue("@showtimes", showStr.Length == 0 ? DBNull.Value.ToString() : showStr);
                            string raStr = "";
                            if (rids != null)
                            {
                                foreach (int ra in rids) { raStr += ra; raStr += ","; }
                            }
                            insert_sqlCommand.Parameters.AddWithValue("@ratings", raStr.Length == 0 ? DBNull.Value.ToString() : raStr); 
                            insert_sqlCommand.Parameters.AddWithValue("@officialurl", r.officialUrl == null || r.officialUrl.Length == 0 ? DBNull.Value.ToString() : r.officialUrl);
                            insert_sqlCommand.Parameters.AddWithValue("@animation", ("true".Equals(r.animation) ? 1 : 0));
                            
                            rid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        Console.WriteLine("\tListing with tmsId " + r.tmsId + " already exists.");

                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table Listing");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            
            return rid;
        }

        private List<int> storeRatings(List<model.Rating> rs, SqlConnection sqlConn)
        {
            List<int> rsids = null;
            if (rs != null || rs.Count > 0)
            {
                foreach (model.Rating rt in rs)
                {
                    int rsid = storeOneRating(rt, sqlConn);
                    if (rsid > 0)
                    {
                        if (rsids == null) { rsids = new List<int>(); }
                        rsids.Add(rsid);
                    }
                }
            }
            return rsids;
        }

        private int storeOneRating(model.Rating rt, SqlConnection sqlConn)
        {
            int rsid = -1;
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.Rating where body like @body AND code like @code", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@body", rt.body == null || rt.body.Length == 0 ? DBNull.Value.ToString() : rt.body);
                    sqlCommand.Parameters.AddWithValue("@code", rt.code == null || rt.code.Length == 0 ? DBNull.Value.ToString() : rt.code);
                    int rtcount = (int)sqlCommand.ExecuteScalar();
                    sqlConn.Close();
                    if (rtcount > 0)
                    {
                        using (SqlCommand fetch_sqlCommand = new SqlCommand("SELECT id from dbo.Rating where body like @body and code like @code", sqlConn))
                        {
                            sqlConn.Open();
                            fetch_sqlCommand.Parameters.AddWithValue("@body", rt.body == null || rt.body.Length == 0 ? DBNull.Value.ToString() : rt.body);
                            fetch_sqlCommand.Parameters.AddWithValue("@code", rt.code == null || rt.code.Length == 0 ? DBNull.Value.ToString() : rt.code);
                            using (SqlDataReader reader = fetch_sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    rsid = reader.GetInt32(reader.GetOrdinal("id"));
                                }
                            }
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.Rating (body, code) VALUES (@body, @code) SELECT CAST(scope_identity() AS int)", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@body", rt.body == null || rt.body.Length == 0 ? DBNull.Value.ToString() : rt.body);
                            insert_sqlCommand.Parameters.AddWithValue("@code", rt.code == null || rt.code.Length == 0 ? DBNull.Value.ToString() : rt.code);
                            rsid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table Rating");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            return rsid;
        }

        private int storeQualityRaing(model.QualityRating qr, SqlConnection sqlConn)
        {
            int qrid = -1;
            
            try
            {
                float ratingfl = 0.0f;
                if (qr.ratingsValue != null && qr.ratingsValue.Length > 0) { ratingfl = float.Parse(qr.ratingsValue, System.Globalization.CultureInfo.InvariantCulture.NumberFormat); }
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.QualityRatings where ratingsbody like @ratingsbody AND value = @val", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@ratingsbody", qr.ratingsBody == null || qr.ratingsBody.Length == 0 ? DBNull.Value.ToString() : qr.ratingsBody);
                    sqlCommand.Parameters.AddWithValue("@val", ratingfl);
                    int qrcount = (int)sqlCommand.ExecuteScalar();
                    sqlConn.Close();
                    if (qrcount > 0)
                    {
                        using (SqlCommand fetch_sqlCommand = new SqlCommand("SELECT id from dbo.QualityRatings where ratingsbody like @ratingsbody AND value = @val", sqlConn))
                        {
                            sqlConn.Open();
                            fetch_sqlCommand.Parameters.AddWithValue("@ratingsbody", qr.ratingsBody == null || qr.ratingsBody.Length == 0 ? DBNull.Value.ToString() : qr.ratingsBody);
                            fetch_sqlCommand.Parameters.AddWithValue("@val", ratingfl);
                            using (SqlDataReader reader = fetch_sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    qrid = reader.GetInt32(reader.GetOrdinal("id"));
                                }
                            }
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.QualityRatings (ratingsbody, value) VALUES (@ratingsbody, @val) SELECT CAST(scope_identity() AS int)", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@ratingsbody", qr.ratingsBody == null || qr.ratingsBody.Length == 0 ? DBNull.Value.ToString() : qr.ratingsBody);
                            insert_sqlCommand.Parameters.AddWithValue("@val", ratingfl);
                            qrid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table QualityRatings");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            return qrid;
        }

        private List<int> storeShowTimes(List<model.Showtime> stl, SqlConnection sqlConn)
        {
            List<int> showtimeids = null;
            foreach (model.Showtime s in stl)
            {
                if (s != null)
                {
                    int sid = storeOneShowTime(s, sqlConn);
                    if (sid > 0)
                    {
                        if (showtimeids == null) { showtimeids = new List<int>(); }
                        showtimeids.Add(sid);
                    }
                }
            }
            return showtimeids;
        }

        private int storeOneShowTime(model.Showtime s, SqlConnection sqlConn)
        {
            int sid = -1;
            model.Theatre t = s.theatre;
            int tid = storeTheatre(t, sqlConn);
            
            
            try
            {
                DateTime? dt = (DateTime?)null;
                if (s.dateTime != null && s.dateTime.Length > 0) { dt = DateTime.ParseExact(s.dateTime, "yyyy-MM-ddTHH:mm", null); }
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.Showtime where datetime = @datetime AND theatreid = @tid AND ticketuri like @turi", sqlConn))
                {
                    sqlConn.Open();
                    if (dt != null)
                    {
                        SqlParameter dateTimeParam = new SqlParameter("@datetime", SqlDbType.DateTime);
                        dateTimeParam.Value = dt;
                        sqlCommand.Parameters.Add(dateTimeParam);
                    }
                    else
                    {
                        sqlCommand.Parameters.AddWithValue("@datetime", DBNull.Value);
                    }
                    
                    sqlCommand.Parameters.AddWithValue("@tid", (tid > 0) ? tid : 1);
                    sqlCommand.Parameters.AddWithValue("@turi", s.ticketURI == null || s.ticketURI.Length == 0 ? DBNull.Value.ToString() : s.ticketURI);
                    object result = (int)sqlCommand.ExecuteScalar();
                    int scount = (result == null) ? 0 : (int)result;
                    sqlConn.Close();
                    if (scount > 0)
                    {
                        using (SqlCommand fetch_sqlCommand = new SqlCommand("SELECT id from dbo.Showtime where datetime = @datetime AND theatreid = @tid AND ticketuri like @turi", sqlConn))
                        {
                            sqlConn.Open();
                            if (dt != null)
                            {
                                SqlParameter fetch_dateTimeParam = new SqlParameter("@datetime", SqlDbType.DateTime);
                                fetch_dateTimeParam.Value = dt;
                                fetch_sqlCommand.Parameters.Add(fetch_dateTimeParam);
                            }
                            else
                            {
                                fetch_sqlCommand.Parameters.AddWithValue("@datetime", DBNull.Value);
                            }
                            
                            fetch_sqlCommand.Parameters.AddWithValue("@tid", (tid > 0) ? tid : 1);
                            fetch_sqlCommand.Parameters.AddWithValue("@turi", s.ticketURI == null || s.ticketURI.Length == 0 ? DBNull.Value.ToString() : s.ticketURI);
                            using (SqlDataReader reader = fetch_sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    sid = reader.GetInt32(reader.GetOrdinal("id"));
                                }
                            }
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.Showtime (datetime, barg, ticketuri, quals, theatreid) VALUES (@datetime, @barg, @ticketuri, @quals, @theatreid); SELECT CAST(scope_identity() AS int);", sqlConn))
                        {
                            sqlConn.Open();
                            if (dt != null)
                            {
                                SqlParameter insert_dateTimeParam = new SqlParameter("@datetime", SqlDbType.DateTime);
                                insert_dateTimeParam.Value = dt;
                                insert_sqlCommand.Parameters.Add(insert_dateTimeParam);
                            }
                            else
                            {
                                insert_sqlCommand.Parameters.AddWithValue("@datetime", DBNull.Value);
                            }
                            
                            insert_sqlCommand.Parameters.AddWithValue("@barg", s.barg ? 1 : 0);
                            insert_sqlCommand.Parameters.AddWithValue("@quals", s.quals == null || s.quals.Length == 0 ? DBNull.Value.ToString() : s.quals);
                            insert_sqlCommand.Parameters.AddWithValue("@theatreid", (tid > 0) ? tid : 1);
                            insert_sqlCommand.Parameters.AddWithValue("@ticketuri", s.ticketURI == null || s.ticketURI.Length == 0 ? DBNull.Value.ToString() : s.ticketURI);
                            
                            sid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table ShowTime");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            return sid;
        }

        private int storeTheatre(model.Theatre t, SqlConnection sqlConn)
        {
            int tid = -1;
            if (t == null || t.theatreId <= 0) { return tid; }
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.Theatre where id = @id", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@id", t.theatreId);
                    object result = sqlCommand.ExecuteScalar();
                    int tcount = (result == null) ? 0 : (int)result;
                    sqlConn.Close();
                    if (tcount > 0)
                    {
                        tid = t.theatreId;
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.Theatre (id, name) VALUES (@id, @name)", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@id", t.theatreId);
                            insert_sqlCommand.Parameters.AddWithValue("@name", t.name == null || t.name.Length == 0 ? DBNull.Value.ToString() : t.name);
                            int numrows = insert_sqlCommand.ExecuteNonQuery();
                            sqlConn.Close();
                            if (numrows < 0)
                            {
                                Console.WriteLine("\r\n\t Problem in inserting into Theatre table values : " + t.ToString());
                                tid = -1;
                            }
                            else
                            {
                                tid = t.theatreId;
                            }
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table Theatre");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            return tid;
        }

        

        private int storePreferredImage(model.PreferredImage pi, SqlConnection sqlConn)
        {
            int pid = -1;
            model.Caption c = pi.caption;
            int cid = -1;
            if (c != null)
            {
                cid = storeCaption(c, sqlConn);
            }
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.PreferredImage where uri like @uri AND pitext like @pitext", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@uri", pi.uri == null || pi.uri.Length == 0 ? DBNull.Value.ToString() : pi.uri);
                    sqlCommand.Parameters.AddWithValue("@pitext", pi.text == null || pi.text.Length == 0 ? DBNull.Value.ToString() : pi.text);
                    
                    object result = sqlCommand.ExecuteScalar();
                    
                    int picount = (result == null) ? 0 : (int)result;
                    
                    sqlConn.Close();
                    if (picount > 0)
                    {
                        using (SqlCommand fetch_sqlCommand = new SqlCommand("SELECT id from dbo.PreferredImage where uri like @uri AND pitext like @pitext", sqlConn))
                        {
                            sqlConn.Open();
                            fetch_sqlCommand.Parameters.AddWithValue("@uri", pi.uri == null || pi.uri.Length == 0 ? DBNull.Value.ToString() : pi.uri);
                            fetch_sqlCommand.Parameters.AddWithValue("@pitext", pi.text == null || pi.text.Length == 0 ? DBNull.Value.ToString() : pi.text);
                            using (SqlDataReader reader = fetch_sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    pid = reader.GetInt32(reader.GetOrdinal("id"));
                                }
                            }
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.PreferredImage (uri, captionid, width, height, category, pitext, isprimary) VALUES (@uri, @captionid, @width, @height, @category, @pitext, @isprimary); SELECT CAST(scope_identity() AS int);", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@uri", pi.uri == null || pi.uri.Length == 0 ? DBNull.Value.ToString() : pi.uri);
                            insert_sqlCommand.Parameters.AddWithValue("@captionid", (cid > 0) ? cid : 1);
                            insert_sqlCommand.Parameters.AddWithValue("@width", pi.width == null || pi.width.Length == 0 ? DBNull.Value.ToString() : pi.width);
                            insert_sqlCommand.Parameters.AddWithValue("@height", pi.height == null || pi.height.Length == 0 ? DBNull.Value.ToString() : pi.height);
                            insert_sqlCommand.Parameters.AddWithValue("@category", pi.category == null || pi.category.Length == 0 ? DBNull.Value.ToString() : pi.category);
                            insert_sqlCommand.Parameters.AddWithValue("@pitext", pi.text == null || pi.text.Length == 0 ? DBNull.Value.ToString() : pi.text);
                            insert_sqlCommand.Parameters.AddWithValue("@isprimary", "true".Equals(pi.primary) ? 1 : 0);
                            
                            pid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table PreferredImage");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }
            

            return -1;
        }

        private int storeCaption(model.Caption c, SqlConnection sqlConn)
        {
            int captionid = -1;
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from dbo.Caption where content like @content AND lang like @lang", sqlConn))
                {
                    sqlConn.Open();
                    sqlCommand.Parameters.AddWithValue("@content", c.content == null || c.content.Length == 0 ? DBNull.Value.ToString() : c.content);
                    sqlCommand.Parameters.AddWithValue("@lang", c.lang == null || c.lang.Length == 0 ? DBNull.Value.ToString() : c.lang);
                    object result = sqlCommand.ExecuteScalar();
                    int captionCount = (result == null) ? 0 : (int)result;
                    sqlConn.Close();
                    if (captionCount > 0)
                    {
                        using (SqlCommand fetch_sqlCommand = new SqlCommand("SELECT id from dbo.Caption where content like @content AND lang like @lang", sqlConn))
                        {
                            sqlConn.Open();
                            fetch_sqlCommand.Parameters.AddWithValue("@content", c.content == null || c.content.Length == 0 ? DBNull.Value.ToString() : c.content);
                            fetch_sqlCommand.Parameters.AddWithValue("@lang", c.lang == null || c.lang.Length == 0 ? DBNull.Value.ToString() : c.lang);
                            using (SqlDataReader reader = fetch_sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    captionid = reader.GetInt32(reader.GetOrdinal("id"));
                                }
                            }
                            sqlConn.Close();
                        }
                    }
                    else
                    {
                        using (SqlCommand insert_sqlCommand = new SqlCommand("INSERT INTO dbo.Caption (lang, content) VALUES (@lang, @content) SELECT CAST(scope_identity() AS int)", sqlConn))
                        {
                            sqlConn.Open();
                            insert_sqlCommand.Parameters.AddWithValue("@lang", c.lang == null || c.lang.Length == 0 ? DBNull.Value.ToString() : c.lang);
                            insert_sqlCommand.Parameters.AddWithValue("@content", c.content == null || c.content.Length == 0 ? DBNull.Value.ToString() : c.content);
                            captionid = (int)insert_sqlCommand.ExecuteScalar();
                            sqlConn.Close();
                        }
                    }
                }
            }
            catch (System.Exception se)
            {
                sqlConn.Close();
                Console.WriteLine("\r\n\t Exception with table Caption");
                Console.WriteLine("\r\n\t Exception Source: " + se.Source +  "\r\n\t Exception Message: " + se.Message);
                //throw se;
                return -1;
            }

            return captionid;
        }

        public void storeInDatabase(string name)
        {
            List<model.RootObject> rootObjects = readObjectsFromXML(name);
            List<int> insertedRecordIds = new List<int>();
            if (rootObjects != null && rootObjects.Count > 0)
            {
                using(SqlConnection sqlConn = new SqlConnection(connectionStr)) {
                    foreach (model.RootObject r in rootObjects)
                    {
                        int ret = store(r, sqlConn);
                        if (ret > 0) { insertedRecordIds.Add(ret); }
                    }
                }

                Console.WriteLine("\r\n\t Total No. of records inserted = " + insertedRecordIds.Count);
                if (insertedRecordIds.Count > 0)
                {
                    Console.Write("\r\n\t ");
                    foreach (int recId in insertedRecordIds) { Console.Write(recId + ", "); }
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("\r\n\t XML Objects read are empty.");
            }

        }
    }
}
