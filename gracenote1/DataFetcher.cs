﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Xml.Serialization;
namespace gracenote1.data
{
    public class DataFetcher : DataWorkerBase
    {
        private static HashSet<string> param_names = new HashSet<string> { 
            "startDate", "numDays", "imageSize", "imageText", "radius", "units", "lng", "lat", "zip", "name" };
        private static HashSet<string> essential_param_names = new HashSet<string> { "zip" };
        private Dictionary<string, string> urlparams = null;
        private string fileName;
        private string jsonResponse;
        private string[] passedparams;
        public string[] RawParams { 
            get
            {
                return passedparams;
            }
            set 
            {
                passedparams = null;
                if (urlparams != null) { urlparams.Clear(); }
                fileName = null;
                jsonResponse = null;
                this.passedparams = value;
            }
        }

        private void setFileName()
        {
            if (fileName != null && fileName.Length > 0)
            {
                return;
            }

            if (urlparams != null)
            {
                if (!urlparams.ContainsKey("name"))
                {
                    if (!urlparams.ContainsKey("zip"))
                    {
                        fileName = null;
                    }
                    else
                    {
                        fileName = urlparams["zip"];
                    }
                }
                else
                {
                    fileName = urlparams["name"];
                }
            }
            else
            {
                fileName = null;
            }
        }

        private Tuple<bool, string> checkURLParams()
        {
            if (passedparams == null || passedparams.Length == 0)
            {
                return Tuple.Create(false, "\r\n\tNo params passed to fetch. Set RawParams first.");
            }
            StringBuilder retmsg = new StringBuilder();
            for (int i = 1; i < passedparams.Length; i++)
            {
                string[] psplit = passedparams[i].Split('=');
                if (psplit.Length < 2)
                {
                    continue;
                }
                if (!param_names.Contains(psplit[0]))
                {
                    retmsg.Append("\r\n\tWARN: Illegal param name: ").Append(psplit[0]);
                }
                else
                {
                    if (this.urlparams == null)
                    {
                        this.urlparams = new Dictionary<string, string>();
                    }
                    urlparams.Add(psplit[0], psplit[1]);
                }
            }

            Tuple<bool, string> ret_tuple;
            if (urlparams == null || urlparams.Count == 0)
            {
                retmsg.Append("\r\n\tCRITICAL: No. of legal params found = 0!");
                ret_tuple = Tuple.Create(false, retmsg.ToString());
                return ret_tuple;
            }
            else
            {
                if (!urlparams.ContainsKey("startDate"))
                {
                    string todayWithFormat = DateTime.Today.ToString("yyyy-MM-dd");
                    urlparams.Add("startDate", todayWithFormat);
                    retmsg.Append("\r\n\tparam startDate is missing. Adding as today's date by default.");
                }
                
                ret_tuple = Tuple.Create(true, retmsg.ToString());
                return ret_tuple;
            }
        }

        private Tuple<bool, string> checkEssentialParams() 
        {
            Tuple<bool, string> ret_tuple;
            if (this.urlparams == null || this.urlparams.Count == 0)
            {
                ret_tuple = Tuple.Create(false, "\r\n\tNo. of parameters found = 0. Nothing to check.");
                return ret_tuple;
            }
            bool allessentialsfound = true;
            StringBuilder sb = new StringBuilder();
            foreach (string essentialparam in essential_param_names)
            {
                if (!this.urlparams.ContainsKey(essentialparam)) 
                {
                    allessentialsfound = false;
                    sb.Append("\r\n\tEssential param ").Append(essentialparam).Append(" Not found.");
                }
            }

            return Tuple.Create(allessentialsfound, sb.ToString());
        }

        private string buildURL()
        {
            if (this.urlparams != null && this.urlparams.Count > 0)
            {
                string apikey = ReadSetting("apikey_onconnect");
                string apiurl = ReadSetting("apiurl_onconnect");
                StringBuilder urlBuilder = new StringBuilder();
                urlBuilder.Append(apiurl).Append("?api_key=").Append(apikey);
                foreach (KeyValuePair<string, string> paramvalue in urlparams)
                {
                    urlBuilder.Append("&").Append(paramvalue.Key).Append("=").Append(paramvalue.Value);
                }
                return urlBuilder.ToString();
                
            }
            else
            {
                return null;
            }
        }

        private bool storeJSONOnDisk()
        {
            setFileName();
            if (fileName == null || fileName.Length == 0)
            {
                Console.WriteLine("\r\n\t File name is blank or null.");
                return false;
            }

            if (jsonResponse == null || jsonResponse.Length == 0)
            {
                Console.WriteLine("\r\n\t JSON Content is blank or null.");
                return false;
            }

            string relativePath = ReadSetting("storage");
            string completePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
            
            if (!System.IO.Directory.Exists(completePath))
            {
                Console.WriteLine("Directory " + completePath + " does not exists. Attempting to create it.");
                try
                {
                    System.IO.Directory.CreateDirectory(completePath);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("\r\n\tException creating directory: " + e.Source + ", \r\n\t" + e.Data);
                    Console.WriteLine("Unable to store json file.");
                    return false;
                }
            }

            completePath += fileName;
            completePath += ".json";
            if (System.IO.File.Exists(completePath))
            {
                Console.WriteLine("File with name " + fileName + " already exists. Overwriting file.");
            }
            System.IO.File.WriteAllText(completePath, jsonResponse);
            Console.WriteLine("\tJSON stored at path :" + completePath);
            return true;
        }
        
        private async Task<List<model.RootObject>> getData()
        {
            
            Tuple<bool, string> checkedResult = checkURLParams();
            Console.WriteLine(checkedResult.Item2);
            if (checkedResult.Item1 == false)
            {
                return null;
            }

            Tuple<bool, string> essentialCheckedResult = checkEssentialParams();
            if (essentialCheckedResult.Item1 == false)
            {
                Console.WriteLine(essentialCheckedResult.Item2);
                return null;
            }

            string wellformed_url = buildURL();
            if (wellformed_url == null || wellformed_url.Length == 0)
            {
                Console.WriteLine("\r\n\tSome problem occurred in constructing the URL.");
                return null;
            }

            Console.WriteLine("\tREST URL (GET): " + wellformed_url);
            List<model.RootObject> responseListings = null;
            
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(wellformed_url);
                Console.WriteLine("\tStatus code: " + response.StatusCode);
                if (response.IsSuccessStatusCode)
                {
                    
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    jsonResponse = jsonString.Result;
                    responseListings = JsonConvert.DeserializeObject<List<model.RootObject>>(jsonString.Result);
                    if (!storeJSONOnDisk())
                    {
                        Console.WriteLine("\r\n\tCould not save file " + fileName + " on disk.");
                    }
                }
                else
                {
                    Console.WriteLine("\tRequest Failed. Code " + response.StatusCode);
                    return responseListings;
                }
            }
            
            return responseListings;
        }

        public bool getDataAndStoreAsXML()
        {
            Task<List<model.RootObject>> response = getData();
            if (response.Result != null && response.Result.Count > 0)
            {
                setFileName();
                if (fileName == null || fileName.Length == 0)
                {
                    Console.WriteLine("\r\n\t File name is blank or null.");
                    return false;
                }
                
                string relativePath = ReadSetting("storage");
                string xmlStoragePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
                if (!System.IO.Directory.Exists(xmlStoragePath))
                {
                    Console.WriteLine("\tDirectory " + xmlStoragePath + " does not exists. Attempting to create it.");
                    try
                    {
                        System.IO.Directory.CreateDirectory(xmlStoragePath);
                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine("\r\n\tException creating directory \r\n\t Exception Source:" + e.Source + ", \r\n\t Exception Message:" + e.Message);
                        Console.WriteLine("\tUnable to store json file.");
                        return false;
                    }
                }

                xmlStoragePath += fileName;
                xmlStoragePath += ".xml";
                if (System.IO.File.Exists(xmlStoragePath))
                {
                    Console.WriteLine("\tFile with name " + fileName + " already exists. Overwriting file.");
                }
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<model.RootObject>));
                    using (System.IO.TextWriter writer = new System.IO.StreamWriter(xmlStoragePath))
                    {
                        List<model.RootObject> dsrzObjects = response.Result;
                        serializer.Serialize(writer, dsrzObjects);
                    }
                    Console.WriteLine("\tXML stored at path :" + xmlStoragePath);
                    return true;
                }
                catch (System.Exception se)
                {
                    Console.WriteLine("\r\n\t Exception in writing XML file at location \r\n\t" + xmlStoragePath);
                    Console.WriteLine("\r\n\t Exception Source: " + se.Source + "\r\n\t Exception Message: " + se.Message);
                    return false;
                }
                
            }
            else
            {
                Console.WriteLine("\tNo objects found to store in XML.");
                return false;
            }
        }
    }
}
