namespace gracenote1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Captions",
                c => new
                    {
                        content = c.String(nullable: false, maxLength: 128),
                        lang = c.String(),
                    })
                .PrimaryKey(t => t.content);
            
            CreateTable(
                "dbo.PreferredImages",
                c => new
                    {
                        uri = c.String(nullable: false, maxLength: 128),
                        width = c.String(),
                        height = c.String(),
                        category = c.String(),
                        text = c.String(),
                        primary = c.String(),
                        caption_content = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.uri)
                .ForeignKey("dbo.Captions", t => t.caption_content)
                .Index(t => t.caption_content);
            
            CreateTable(
                "dbo.QualityRatings",
                c => new
                    {
                        ratingsBody = c.String(nullable: false, maxLength: 128),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.ratingsBody);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        body = c.String(nullable: false, maxLength: 128),
                        code = c.String(),
                        RootObject_tmsId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.body)
                .ForeignKey("dbo.RootObjects", t => t.RootObject_tmsId)
                .Index(t => t.RootObject_tmsId);
            
            CreateTable(
                "dbo.RootObjects",
                c => new
                    {
                        tmsId = c.String(nullable: false, maxLength: 128),
                        rootId = c.String(),
                        subType = c.String(),
                        title = c.String(),
                        releaseYear = c.Int(nullable: false),
                        releaseDate = c.String(),
                        titleLang = c.String(),
                        descriptionLang = c.String(),
                        entityType = c.String(),
                        longDescription = c.String(),
                        shortDescription = c.String(),
                        runTime = c.String(),
                        officialUrl = c.String(),
                        animation = c.String(),
                        preferredImage_uri = c.String(maxLength: 128),
                        qualityRating_ratingsBody = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.tmsId)
                .ForeignKey("dbo.PreferredImages", t => t.preferredImage_uri)
                .ForeignKey("dbo.QualityRatings", t => t.qualityRating_ratingsBody)
                .Index(t => t.preferredImage_uri)
                .Index(t => t.qualityRating_ratingsBody);
            
            CreateTable(
                "dbo.Showtimes",
                c => new
                    {
                        ticketURI = c.String(nullable: false, maxLength: 128),
                        dateTime = c.String(),
                        barg = c.Boolean(nullable: false),
                        quals = c.String(),
                        theatre_id = c.String(maxLength: 128),
                        RootObject_tmsId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ticketURI)
                .ForeignKey("dbo.Theatres", t => t.theatre_id)
                .ForeignKey("dbo.RootObjects", t => t.RootObject_tmsId)
                .Index(t => t.theatre_id)
                .Index(t => t.RootObject_tmsId);
            
            CreateTable(
                "dbo.Theatres",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Showtimes", "RootObject_tmsId", "dbo.RootObjects");
            DropForeignKey("dbo.Showtimes", "theatre_id", "dbo.Theatres");
            DropForeignKey("dbo.Ratings", "RootObject_tmsId", "dbo.RootObjects");
            DropForeignKey("dbo.RootObjects", "qualityRating_ratingsBody", "dbo.QualityRatings");
            DropForeignKey("dbo.RootObjects", "preferredImage_uri", "dbo.PreferredImages");
            DropForeignKey("dbo.PreferredImages", "caption_content", "dbo.Captions");
            DropIndex("dbo.Showtimes", new[] { "RootObject_tmsId" });
            DropIndex("dbo.Showtimes", new[] { "theatre_id" });
            DropIndex("dbo.RootObjects", new[] { "qualityRating_ratingsBody" });
            DropIndex("dbo.RootObjects", new[] { "preferredImage_uri" });
            DropIndex("dbo.Ratings", new[] { "RootObject_tmsId" });
            DropIndex("dbo.PreferredImages", new[] { "caption_content" });
            DropTable("dbo.Theatres");
            DropTable("dbo.Showtimes");
            DropTable("dbo.RootObjects");
            DropTable("dbo.Ratings");
            DropTable("dbo.QualityRatings");
            DropTable("dbo.PreferredImages");
            DropTable("dbo.Captions");
        }
    }
}
