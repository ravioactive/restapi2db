﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gracenote1;
using System.Xml.Serialization;

namespace gracenote1.model
{
    public class Caption
    {
        [XmlElement("content")]
        public string content { get; set; }
        [XmlElement("lang")]
        public string lang { get; set; }

        override public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("content:").Append(content).Append(", ");
            sb.Append("lang:").Append(lang).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class PreferredImage
    {
        [XmlElement("uri")]
        public string uri { get; set; }
        [XmlElement("caption")]
        public Caption caption { get; set; }
        [XmlElement("width")]
        public string width { get; set; }
        [XmlElement("height")]
        public string height { get; set; }
        [XmlElement("category")]
        public string category { get; set; }
        [XmlElement("text")]
        public string text { get; set; }
        [XmlElement("primary")]
        public string primary { get; set; }
        
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("uri:").Append(uri).Append(", ");
            sb.Append("width:").Append(width).Append(", ");
            sb.Append("height:").Append(height).Append(", ");
            sb.Append("caption:");
            if (caption != null)
            {
                sb.Append(caption.ToString()).Append(", ");
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            sb.Append("category:").Append(category).Append(", ");
            sb.Append("text:").Append(text).Append(", ");
            sb.Append("primary:").Append(primary).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class Theatre
    {
        [XmlElement("id")]
        public int theatreId { get; set; }
        [XmlElement("name")]
        public string name { get; set; }
        override public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("id:").Append(theatreId).Append(", ");
            sb.Append("name:").Append(name).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class Showtime
    {
        [XmlElement("dateTime")]
        public string dateTime { get; set; }
        [XmlElement("theatre")]
        public Theatre theatre { get; set; }
        [XmlElement("barg")]
        public bool barg { get; set; }
        [XmlElement("ticketURI")]
        public string ticketURI { get; set; }
        [XmlElement("quals")]
        public string quals { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Theatre:");
            if (theatre != null)
            {
                sb.Append(theatre.ToString()).Append(", ");
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            
            sb.Append("dateTime:").Append(dateTime).Append(", ");
            sb.Append("barg:").Append(barg).Append(", ");
            sb.Append("ticketURI:").Append(ticketURI).Append(", ");
            sb.Append("quals:").Append(quals).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class QualityRating
    {
        [XmlElement("ratingsBody")]
        public string ratingsBody { get; set; }
        [XmlElement("value")]
        public string ratingsValue { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ratingsBody:").Append(ratingsBody).Append(", ");
            sb.Append("code:").Append(ratingsValue).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class Rating
    {
        [XmlElement("body")]
        public string body { get; set; }
        [XmlElement("code")]
        public string code { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("body:").Append(body).Append(", ");
            sb.Append("code:").Append(code).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }

    public class RootObject
    {
        [XmlElement("tmsId")]
        public string tmsId { get; set; }
        [XmlElement("rootId")]
        public string rootId { get; set; }
        [XmlElement("subType")]
        public string subType { get; set; }
        [XmlElement("title")]
        public string title { get; set; }
        [XmlElement("releaseYear")]
        public int releaseYear { get; set; }
        [XmlElement("releaseDate")]
        public string releaseDate { get; set; }
        [XmlElement("titleLang")]
        public string titleLang { get; set; }
        [XmlElement("descriptionLang")]
        public string descriptionLang { get; set; }
        [XmlElement("entityType")]
        public string entityType { get; set; }
        [XmlElement("genres")]
        public List<string> genres { get; set; }
        [XmlElement("longDescription")]
        public string longDescription { get; set; }
        [XmlElement("shortDescription")]
        public string shortDescription { get; set; }
        [XmlElement("topCast")]
        public List<string> topCast { get; set; }
        [XmlElement("directors")]
        public List<string> directors { get; set; }
        [XmlElement("advisories")]
        public List<string> advisories { get; set; }
        [XmlElement("runTime")]
        public string runTime { get; set; }
        
        [XmlElement("preferredImage")]
        public PreferredImage preferredImage { get; set; }

        [XmlElement("showtimes")]
        public List<Showtime> showtimes { get; set; }
        [XmlElement("qualityRating")]
        public QualityRating qualityRating { get; set; }
        [XmlElement("ratings")]
        public List<Rating> ratings { get; set; }
        [XmlElement("officialUrl")]
        public string officialUrl { get; set; }
        [XmlElement("animation")]
        public string animation { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("tmsId:").Append(tmsId).Append(", ");
            sb.Append("rootId:").Append(rootId).Append(", ");
            sb.Append("subType:").Append(subType).Append(", ");
            sb.Append("title:").Append(title).Append(", ");
            sb.Append("releaseYear:").Append(releaseYear).Append(", ");
            sb.Append("releaseDate:").Append(releaseDate).Append(", ");
            sb.Append("titleLang:").Append(titleLang).Append(", ");
            sb.Append("descriptionLang:").Append(descriptionLang).Append(", ");
            sb.Append("entityType:").Append(entityType).Append(", ");
            sb.Append("genres: [");
            if (genres != null)
            {
                foreach (string g in genres)
                {
                    sb.Append(g).Append(", ");
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            sb.Append("], ");

            sb.Append("longDescription:").Append(longDescription).Append(", ");
            sb.Append("shortDescription:").Append(shortDescription).Append(", ");
            
            sb.Append("topCast: [");
            if (topCast != null)
            {
                foreach (string c in topCast)
                {
                    sb.Append(c).Append(", ");
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            
            sb.Append("], ");
            
            sb.Append("directors: [");
            if (directors != null)
            {
                foreach (string d in directors)
                {
                    sb.Append(d).Append(", ");
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            sb.Append("], ");
            
            sb.Append("advisories: [");
            if (advisories != null)
            {
                foreach (string a in advisories)
                {
                    sb.Append(a).Append(", ");
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            
            sb.Append("], ");
            sb.Append("runTime:").Append(runTime).Append(", ");
            
            sb.Append("preferredImage:");
            if (preferredImage != null)
            {
                sb.Append(preferredImage.ToString()).Append(", ");
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            
            sb.Append("showtimes: [");
            if (showtimes != null)
            {
                foreach (Showtime s in showtimes)
                {
                    if (s != null)
                    {
                        sb.Append(s.ToString()).Append(", ");
                    }
                    else
                    {
                        sb.Append("null").Append(", ");
                    }
                    
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            sb.Append("], ");

            sb.Append("qualityRating:");
            if (qualityRating != null)
            {
                sb.Append(qualityRating.ToString()).Append(", ");
            }
            else
            {
                sb.Append("null").Append(", ");
            }

            sb.Append("ratings: [");
            if (ratings != null)
            {
                foreach (Rating r in ratings)
                {
                    if (r != null)
                    {
                        sb.Append(r.ToString()).Append(", ");
                    }
                    else
                    {
                        sb.Append("null").Append(", ");
                    }
                }
            }
            else
            {
                sb.Append("null").Append(", ");
            }
            sb.Append("], ");
            sb.Append("officialUrl:").Append(officialUrl).Append(", ");
            sb.Append("animation:").Append(animation).Append(", ");
            sb.Append("\n");
            return sb.ToString();
        }
    }
    
    
}
