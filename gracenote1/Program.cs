﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using gracenote1.data;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace gracenote1
{
    class Program
    {
        static void Main(string[] args)
        {
            DataFetcher df = new DataFetcher();
            DataPusher dp = new DataPusher();
            
            String command;
            Boolean quitNow = false;
            Console.WriteLine(" ======== GRACENOTE ONCONNECT AREA MOVIE LISTINGS API DEMO ======== ");
            Console.WriteLine(" Usage: Simple command line application supports 3 commands:");
            Console.WriteLine(" \r\n 1. fetch zip=12345 ");
            Console.WriteLine(" \t Mandatory args: zip");
            Console.WriteLine(" \t\t zip: US zip code or Canadian postal code. Must supply either zip or coordinates.");
            Console.WriteLine(" \t Optional args: startDate, numDays, imageSize, imageText, radius, units, lng, lat, name");
            Console.WriteLine(" \t\t startDate: date in yyyy-MM-dd format, added by default as today's date if not provided.");
            Console.WriteLine(" \t\t numDays: Number of schedule days. Defaults to 1.");
            Console.WriteLine(" \t\t imageSize: Size of the image referenced by the preferred image URI returned. The default value is Md (medium)");
            Console.WriteLine(" \t\t imageText: Boolean indicating preference for image with or without text. If no image is found matching text preference, next available image will be returned. Defaults to true (prefer images with text).");
            Console.WriteLine(" \t\t radius: Range to search from initial location. Defaults to 5 miles. Maximum 100 mi (160 km).");
            Console.WriteLine(" \t\t units: Unit of measurement for the radius parameter as well as the distance value in response. Defaults to miles.");
            Console.WriteLine(" \t\t lng: Latitude coordinate. Valid values are between -90 and 90. Must supply either zip or coordinates.");
            Console.WriteLine(" \t\t lat: Longitude coordinate. Valid values are between -180 and 180. Must supply either zip or coordinates.");
            Console.WriteLine(" \t\t name: Defaults to value of 'zip' parameter. Name will given to the xml file stored in current_dir/../../../../res/ directory, configurable from app.config. \r\n\t\t You can retrieve this result by the value provided to this param e.g. 'store namevalue'");

            Console.WriteLine(" \r\n 2. store name ");
            Console.WriteLine(" \t Mandatory args: name");
            Console.WriteLine(" \t\t name: name of xml file from which results are to be stored in the localDB instance given along with this project. \r\n\t\t This value will be same as that given (or not, in that case find filename with zipcode.xml) in the fetch command.");
            
            Console.WriteLine(" \r\n 3. quit : to exit ");

            while (!quitNow)
            {
                Console.Write("\r\nEnter Command: ");
                command = Console.ReadLine();
                string[] commandSplits = command.Split(' ');
                int clen = commandSplits.Length;
                if (clen < 1)
                {
                    Console.Write("\r\n\tBlank entered. Please try again.\n");
                    continue;
                }
                
                string maincommand = commandSplits[0];
                
                // Parse command here
                // fetch zipcode=<12345,...> dest=</a/b/../,> name=<myname,...>
                //      size of zipcode has to be >= to dest. dest & name is a completely optional parameter
                //      dest default is resources directory, name default is zipcode itself
                // store name=<myname>
                switch (maincommand)
                {
                    case "fetch":
                        if (commandSplits.Length < 2)
                        {
                            Console.WriteLine("\r\n\tNo params passed for URL. Aborting.");
                            continue;
                        }
                        df.RawParams = commandSplits;
                        bool result = df.getDataAndStoreAsXML();
                        if (result)
                        {
                            Console.WriteLine("\r\nData fetched and stored successfully.");
                        }
                        else
                        {
                            Console.WriteLine("\r\nOperation Unsuccessful.");
                        }
                        
                        break;

                    case "store":
                        if (commandSplits.Length < 2)
                        {
                            Console.WriteLine("\r\n\tParameter 'name' not found. Aborting.");
                            continue;
                        }
                        string name = commandSplits[1];
                        dp.storeInDatabase(name);
                        break;

                    case "quit":
                        Console.WriteLine("Exiting");
                        quitNow = true;
                        break;

                    default:
                        Console.WriteLine("\r\nCannot understand \"" + command + "\"");
                        break;
                }
            }
        } 
    }
}
